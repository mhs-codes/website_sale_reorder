from odoo import http
from odoo.http import request


class MetroReorder(http.Controller):
    @http.route('/shop/reorder', auth='user', methods=['GET'], website=True)
    def do_reorder(self, order_id=False, **kw):
        order_to_clone = request.env['sale.order'].browse(int(order_id))
        order_current = request.website.sale_get_order()
        if order_current:
            order_current.order_line = False
        for line in order_to_clone.website_order_line:
            request.website.sale_get_order(force_create=1)._cart_update(
                product_id=int(line.product_id),
                add_qty=float(line.product_uom_qty),
                set_qty=float(line.product_uom_qty),
            )
        return request.redirect('/shop/cart')
