# -*- encoding: utf-8 -*-

{
    'name': 'Website Sale Reorder',
    'summary': 'Reorder a past eCommerce Order',
    'description': """
    Repeat a previously done Sale from website. 
    """,
    'author': 'Skadoosh Apps',
    'website': 'http://skadooshapps.blogspot.com',
    'version': '1.0',
    'module': 'Metro',
    'depends': [
        'website_sale',
    ],
    'data': [
        'views/templates.xml',
    ],
    'installable': True,
}
